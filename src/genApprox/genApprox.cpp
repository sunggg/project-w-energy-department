#include "llvm/Pass.h"
//#include "llvm/IR/DebugInfo.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfo.h"
#include <fstream>
#include <cstring>
#include <cassert>

using namespace llvm;
namespace {

  struct genApprox: public ModulePass{
    static char ID;

    genApprox() : ModulePass(ID){}; //constructor
    virtual bool runOnModule(Module &M);
    
    virtual void getAnalysisUsage(AnalysisUsage &AU) const{
      AU.addRequired<LoopInfo>();
    }
    using llvm::Pass::doFinalization;
    bool doFinalization(){
      return false;
    }
  }; 
}
char genApprox::ID = 0; // LLVM uses the address of ID. => value doesn't matter
static RegisterPass<genApprox> X("gen", "Generate Approx Function", false, false);
// command line arguement  /  name  /  walks CFG without modifying?   /  analysis pass(ex. dominator tree pass)?
std::set<std::string> approxCandidates;
    
    /* TODO 
     * 1. Now support for 1-D pointer for parametor of function 
     * 2. Return Type matching
     * 3. Type conversion
     *
     * */

    /* 
     * NOTE 
     * Opcode : Load = 27, Store = 28, sext = 35, getelemptr = 29, Alloca = 26 Return : 1 Add : 8
     *
     */

bool loadParams(std::vector<Value*>* paramList, LLVMContext& C)
{
  std::ifstream fileParams;
  fileParams.open("params.txt");
  
  if(fileParams.is_open())
  {
    std::string line;
    while(getline(fileParams, line)){
      Value* params = ConstantFP::get(Type::getFloatTy(C), line);
      paramList->push_back(params);
    }    
    return true;
  }
  else 
    return false;
}

Value* insertLinearRegression(std::vector<Value*>* paramList, std::vector<Value*>* inputList, std::vector<std::pair<Value*, bool> >* outputList, inst_iterator insertPos){
  
  unsigned int inputNum = inputList->size();
  unsigned int outputNum = outputList->size();
  unsigned int paramNum = paramList->size();
  BinaryOperator *multInst[inputNum];
  BinaryOperator *addInst[inputNum];
  std::vector<BinaryOperator*> queue[2];
  LoadInst *ldInst[inputNum];
  SIToFPInst *cldInst[inputNum];
  StoreInst *stInst[outputNum];
  FPToSIInst *cstValue[inputNum];
  Value *retValue = NULL;
  assert(inputNum == paramNum);

  for(unsigned int i=0;i<inputNum;++i){
    AllocaInst* allocaInst = static_cast<AllocaInst*>((*inputList)[i]);
    assert(allocaInst);

    // TODO : For now, assume 1-D pointer
    if(allocaInst->getAllocatedType()->isPointerTy()) 
    {
      LoadInst* ldAddr = new LoadInst((*inputList)[i], "ldaddr", &*insertPos); 
      ldInst[i] = new LoadInst(static_cast<Value*>(ldAddr), "ld_in", &*insertPos);
    }
    else 
      ldInst[i] = new LoadInst(allocaInst, "ld_in", &*insertPos);
    
    if(!ldInst[i]->getType()->isFloatTy())
        cldInst[i] = new SIToFPInst(ldInst[i], Type::getFloatTy((*insertPos).getContext()), "itof", &*insertPos);
  }

  for(unsigned int j=0; j<outputList->size();++j)
  {
    for(unsigned int i=0;i<inputNum;i++)
    {
      if(!ldInst[i]->getType()->isFloatTy())
        multInst[i] = BinaryOperator::Create(Instruction::FMul, cldInst[i], (*paramList)[i], "mul", &*insertPos);
      else
        multInst[i] = BinaryOperator::Create(Instruction::FMul, ldInst[i], (*paramList)[i], "mul", &*insertPos);
      
      queue[0].push_back(multInst[i]);
    }
    int q = 0;
    int addNum=0;
  
    // Sum up all the multiplications
    while(queue[q].size() != 1)
    {
      while(queue[q].size() != 0)
      {
        if(queue[q].size() == 1)
        {
          queue[1-q].push_back(queue[q].back());
          queue[q].pop_back();
        }
        else{
          BinaryOperator *bop1 = queue[q].back();
          queue[q].pop_back();
          BinaryOperator *bop2 = queue[q].back();
          queue[q].pop_back();
          addInst[addNum] = BinaryOperator::Create(Instruction::FAdd, bop1, bop2, "add", &*insertPos);
          queue[1-q].push_back(addInst[addNum++]);
        }
      }
      q = 1-q;
    }

    /* TODO : Support more various types of original functions. now : Integer, Float only */
    // Type conversion to match original function. right now, support for only int.
    AllocaInst* allocaInst = static_cast<AllocaInst*>((*outputList)[j].first);
    if(allocaInst->getAllocatedType()->isPointerTy()) 
    {
      LoadInst* stAddr = new LoadInst((*outputList)[j].first, "staddr", &*insertPos); 
      if(!stAddr->getType()->isFloatTy())
      {
        cstValue[j] = new FPToSIInst(queue[q][0], Type::getInt32Ty((*insertPos).getContext()), "ftoi", &*insertPos);
        stInst[j] = new StoreInst(cstValue[j], stAddr, &*insertPos); 
      }
      else
        stInst[j] = new StoreInst(queue[q][0], stAddr, &*insertPos); 
    }
    else
    {
      if(!(*outputList)[j].first->getType()->isFloatTy())
      {
        cstValue[j] = new FPToSIInst(queue[q][0], Type::getInt32Ty((*insertPos).getContext()), "ftoi", &*insertPos);
        stInst[j] = new StoreInst(cstValue[j], (*outputList)[j].first, &*insertPos); 
        if((*outputList)[j].second == 1) // return
          retValue = cstValue[j];
      }
      else
      {
        stInst[j] = new StoreInst(queue[q][0], (*outputList)[j].first, &*insertPos); 
        if((*outputList)[j].second == 1) // return
          retValue = queue[q][0];
      }
    }
  }
  return retValue;
}

bool checkDuplicity(std::vector<Value*> target, Value* val){
  for(unsigned int i=0; i<target.size(); ++i)
    if(target[i] == val)
      return false;
  
  return true;
}

bool checkDuplicity(std::vector<std::pair<Value*, bool> > target, Value* val){
  for(unsigned int i=0; i<target.size(); ++i)
    if(target[i].first == val)
      return false;
  
  return true;
}

int genApproxFunction(Function* clone){
  errs() << "Approxing ...  " << clone->getName() << "\n\n";
  std::vector<Value*> argList;
  std::vector<Value*> inputList;
  std::vector<std::pair<Value*,bool> > outputList;
  std::pair <Value*, bool> outputPair;
  std::vector<Value *> paramList;

  errs() << "Param Data\n" << loadParams(&paramList, clone->getContext()) << "\n";
  errs() << "\n\n===== Original Program =====\n";

  Instruction* ret_src = NULL;
  for(Function::arg_iterator ait = clone->arg_begin(), aee = clone->arg_end(); ait != aee ; ++ait)
    argList.push_back(ait);

  // Input & Output Analysis
  for(inst_iterator I = inst_begin(clone), E = inst_end(clone); I!=E; ++I)
  {
    if(I->getOpcode() == 1) // return
    {
      Instruction *src_loc = static_cast<Instruction*>(I->getOperand(0));
      if(src_loc->getOpcode() == 27) // load src value
      {
        Instruction *ld_loc = static_cast<Instruction*>(src_loc->getOperand(0));
        if(ld_loc->getOpcode() == 26 && checkDuplicity(outputList, src_loc->getOperand(0))) {
          outputPair.first = src_loc->getOperand(0);
          outputPair.second = true;
          outputList.push_back(outputPair);
          ret_src = ld_loc;
        }
      }
    }
    else if(I->getOpcode() == 28) // store
    {
      Instruction *st_loc = static_cast<Instruction*>(I->getOperand(1));
      // Find output list
      if(st_loc->getOpcode() == 27) // load pointer address 
      {
        Instruction *ld_loc = static_cast<Instruction*>(st_loc->getOperand(0));
        if(ld_loc->getOpcode() == 26 && checkDuplicity(outputList, st_loc->getOperand(0))) {
          outputPair.first = st_loc->getOperand(0);
          outputPair.second = false;
          outputList.push_back(outputPair);
        }
      }
      // Find input list
      else if(st_loc->getOpcode() == 26){ 
        /* TODO : Now support for 1-d pointer param */
        for(std::vector<Value*>::iterator iv = argList.begin(), ev = argList.end() ; iv!=ev; ++iv)
        {
          if(static_cast<Value*>(*iv) == I->getOperand(0)) 
          {
            AllocaInst *allocInst = static_cast<AllocaInst*>(st_loc);
            if(allocInst->getAllocatedType()->isPointerTy())
            {
              for(Value::use_iterator ii = st_loc->use_begin(), ie = st_loc->use_end(); ii!=ie; ++ii)
              {
                Instruction *inst = static_cast<Instruction*>(*ii);
                
                for(Value::use_iterator iii = inst->use_begin(), iie = inst->use_end(); iii!=iie; ++iii)
                {
                  if(Instruction * user_inst = dyn_cast<Instruction>(*iii)){
                    if(user_inst->getOpcode() == 27 && checkDuplicity(inputList, inst->getOperand(0)))
                      inputList.push_back(inst->getOperand(0));
                           
                  }
                }
                
              }
            }
            else
            {
              for(Value::use_iterator ii = st_loc->use_begin(), ie = st_loc->use_end(); ii!=ie; ++ii)
              {
                if(Instruction *inst = dyn_cast<Instruction>(*ii))
                {
                  if(inst->getOpcode() == 27 && checkDuplicity(inputList, inst->getOperand(0))) // load
                    inputList.push_back(inst->getOperand(0));
                }
              }
            }
          }
        }
      }    
    }
    errs() << *I << "\n";
  }

  errs() << "[  Input List  ]\n";
  for(std::vector<Value*>::iterator it = inputList.begin(), ee = inputList.end() ; it != ee ; ++it){
    errs() << **it << "\n";
  }
  errs() << "[  Output List  ]\n";
  for(std::vector<std::pair<Value*, bool> >::iterator it = outputList.begin(), ee = outputList.end() ; it != ee ; ++it){
    errs() << *it->first << " : " << it->second << "\n";
  }
  if(ret_src)
  {
    errs() << "Return : " << *ret_src << "\n";
  }
  

  bool doneStParams = false;
  for(inst_iterator it = inst_begin(clone), ee = inst_end(clone); it!=ee; )
  {
    Instruction *inst = &*it++;
    if(!doneStParams && (inst->getOpcode() == 28) && (inst->getOperand(0) == argList[argList.size()-1])){
      doneStParams = true;
    }
    else if(inst->isTerminator()){
      /* Match the return value type with original function !! Type conversion is required. */ 
      Value* retVal = insertLinearRegression(&paramList, &inputList, &outputList, --it);
      if(retVal != NULL)
        ReturnInst* retInst = ReturnInst::Create(clone->getContext(), retVal, inst); 
      else
      {
        Value* zero = ConstantInt::get(inst->getOperand(0)->getType(), 0, false);
        ReturnInst* retInst = ReturnInst::Create(clone->getContext(), zero, inst); 
      }
 
      inst->eraseFromParent();
      break;
    }
    else if(doneStParams) 
      inst->eraseFromParent();
  }
  
  errs() << "Print generated function\n";
  for(Function::iterator it = clone->begin(), ee = clone->end() ; it != ee; ++it){
    for(BasicBlock::iterator iit = it->begin(), eee = it->end() ; iit != eee; ++iit){
      errs() << *iit << "\n";
    }
  }

  return 0;
}

bool genApprox::runOnModule(Module &M){
  std::ifstream fileFunctionDetection;
  fileFunctionDetection.open("FunctionDetection.csv");
 
  //errs() << M.getName() << "\n";
  //get Approx Candidate lists
  if(fileFunctionDetection.is_open()){
    std::string line;
    while(getline(fileFunctionDetection, line)){
      int i=0;
      while(line[i++] != ',');
      char* funcName = new char[i];
      std::copy(line.begin(), line.begin()+i-1, funcName);
      funcName[i-1] = '\0';
      std::string str_funcName(funcName);
      
      //errs() << line << " ==> " << funcName << ":" << line[i] << "\n";
      if(line[i] == '1' && strcmp(funcName, "main")!=0 )
        approxCandidates.insert(str_funcName);
    }
  }
  else 
  {
    errs() << "Pattern Detection is required!\n";
    exit(1);
  }
  fileFunctionDetection.close();
  
  // Navigate Function Lists and clone function if it is found among candidates.
  for(Module::iterator it = M.begin(), ee = M.end() ; it != ee ; ){
    // approx target
    Function &F = *it;
    if(approxCandidates.count(F.getName().str())>0){
      ValueToValueMapTy VMap;
      Function *clone = CloneFunction(&F, VMap, false);  
      
      genApproxFunction(clone);
      
      errs() << "\n ===== Done generating approx fuction =====\n";
      M.getFunctionList().push_back(clone);
      errs() << "\n ===== Push approx fuction =====\n";

      // Replace all the use of original function calls with approximate function
      F.replaceAllUsesWith(clone);
      ++it;
      // Remove the original one. Actually not a requirement, but for reducing code size.
      F.removeFromParent();
    }
    else
      ++it;

  }

  for(Module::iterator it = M.begin(), ee = M.end() ; it != ee ; ++it){
    // approx target
    Function &F = *it;
    errs() << F << "\n\n";
  }
  
  return 0;
}
