#include "llvm/Pass.h"
//#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfo.h"
#include <fstream>

using namespace llvm;
namespace {

  struct map_detection: public FunctionPass{
    static char ID;
    
    
    map_detection() : FunctionPass(ID){}; //constructor
    virtual bool runOnFunction(Function &F);
    virtual void getAnalysisUsage(AnalysisUsage &AU) const{
      AU.addRequired<LoopInfo>();
    }
    using llvm::Pass::doFinalization;
    bool doFinalization(){
      return false;
    }
  }; 
}
char map_detection::ID = 0; // LLVM uses the address of ID. => value doesn't matter
static RegisterPass<map_detection> X("map_detection", "Detect Map type", false, false);
// command line arguement  /  name  /  walks CFG without modifying?   /  analysis pass(ex. dominator tree pass)?


/* TODO : Need to update the list */
std::string forbiddenFunc[] = { "printf", "fprintf", "scanf", "fscanf", "malloc", "calloc", "free", \
                                "exit", "fflush", "fopen", "memset", "putc", "fputs", "_IO_putc", \
                                "sdkSavePGM", "sdkLoadPGM", "sdkLoadPPM4", "sdkFindFilePath"
                              };

std::set<std::string> forbiddenFuncSet(forbiddenFunc, forbiddenFunc + sizeof(forbiddenFunc) / sizeof(forbiddenFunc[0]));
std::vector<std::pair<Function*, bool> > userDefinedFuncSet;
std::vector<std::pair<Loop*, bool> > detectMapLoopSet;
std::vector<std::pair<Loop*, bool> > detectGatherScatterLoopSet;
std::set <Value*> iterator_list; 
std::ofstream detect_loop, detect_func, detect_report;

bool isUnsafeFunc(Function* F){
  std::vector<std::pair<Function*, bool> >::iterator iit,iie;
  for(iit = userDefinedFuncSet.begin(), iie = userDefinedFuncSet.end() ; iit != iie ; ++iit){
    if(iit->first->getName() == F->getName())
    {
      //errs() << "Now in " << iit->first->getName() << " : " << iit->second <<"\n";
      return iit->second;
    }
  }
  return false;
}

/* TODO
 * Figure out how to get line number from source code
 * When decide whether state input or not, don't compare iterator of for loop yet. 
 *     ex) for(i ~ ) a[k+1] = 1; -> Now consider this as state input .. But not common case  
 *
 * High Priority!!
 * If function returns pointer, NOT map type
 * If function is reduction type, NOT map type
 */


bool checkStateGatherScatter(Instruction* getElemInst, bool* haveStateInput, bool* gather_scatter)
{ 
  GetElementPtrInst* el = dyn_cast<GetElementPtrInst> (getElemInst);
  Instruction *inst = dyn_cast<Instruction> (getElemInst->getOperand(getElemInst->getNumOperands()-1)); // sext
  if(inst && isa<SExtInst>(inst)){
    inst = dyn_cast<Instruction> (inst->getOperand(0)); // load ? binary operation?

    if(isa<LoadInst>(inst))
    {
      Instruction *inst_op0 = dyn_cast<Instruction> (inst->getOperand(0));
      // Gather/Scatter?
      if(inst_op0 && isa<GetElementPtrInst>(inst_op0)){ 
        detect_report<<"\t\tGather/Scatter!\n";
        
        *gather_scatter |= true;
      }
    }
    // State Input?
    else
    {
      *haveStateInput |= true;
      detect_report<<"\t\tState Input!\n";
    }
  }
  return 0;    
}

bool isGlobalVar(Value* var)
{
  GlobalValue* gv = dyn_cast<GlobalValue>(var);
  return isa<GlobalValue>(var) & !isa<Function>(var) & !gv->hasInternalLinkage();
}

bool map_detection::runOnFunction(Function &F){
  LoopInfo &LI = getAnalysis<LoopInfo>();

  //flags for map function
  bool violate        = false;
  bool callUnsafeFunc = false;
  bool haveSideEffect = false;
  bool haveStateInput = false;
  bool gather_scatter = false;
  bool useGlobalVar   = false;

  //flags for map loop
  bool loop_violate        = false;
  bool loop_useGlobalVar   = false;
  bool loop_haveSideEffect = false;
  bool loop_callUnsafeFunc = false;
  bool loop_gather_scatter = false;
  bool loop_haveStateInput = false;

  int loop_count = 0;
  detect_loop.open("LoopDetection.csv", std::ofstream::out | std::ofstream::app);
  detect_func.open("FunctionDetection.csv", std::ofstream::out | std::ofstream::app);
  detect_report.open("DebugInfo.csv", std::ofstream::out | std::ofstream::app);
  Instruction *inst;
  Value *child;

  errs() << F.getName() + "\n";
  detect_report << F.getName().str()+"\n";
  for(Function::iterator Fiter = F.begin(), Fe = F.end() ; Fiter != Fe ; ++Fiter){
    for(BasicBlock::iterator Biter = *Fiter->begin(), Be = *Fiter->end() ; Biter != Be ; ++Biter){
      inst = Biter;
      for(unsigned i=0;i < inst->getNumOperands();++i){
        useGlobalVar |= isGlobalVar(inst->getOperand(i));
        
      }

      if(isa<CallInst>(&(*Biter))){
        CallInst *call = dyn_cast<CallInst>(Biter);
        Function *func = call->getCalledFunction();
        if(func){
          if(forbiddenFuncSet.count(func->getName()))
          {
            haveSideEffect |= true;
            detect_report << "\tUsing I/O library : " + func->getName().str() + "\n";
          }
          else 
            callUnsafeFunc |= isUnsafeFunc(func);  // check if accessing user-defined function is safe or not
        }
        
      }
      /* TODO : What is InvokeInst?? */
      else if(isa<InvokeInst>(&(*Biter))){ 
        detect_report << "\tInvoke Inst\n";
      }
    }
  }

  violate |= haveSideEffect | useGlobalVar | haveStateInput | callUnsafeFunc;
  /* TIP : ++l & calculating end() first is faster than l++ ... related to operator overloading*/
  for(LoopInfo::reverse_iterator l = LI.rbegin(), e = LI.rend(); l != e; ++l){
    Loop *lp = *l;
    loop_count++;
    
    loop_violate        = false;
    loop_gather_scatter = false;
    loop_haveStateInput = false;
    loop_useGlobalVar   = false;
    loop_haveSideEffect = false;
    loop_callUnsafeFunc = false;
    
    detect_report << "\t" << loop_count << "\n";
    /* 
     * NOTE 
     * Opcode : Load = 27, Store = 28, sext = 35, getelemptr = 29, Alloca = 26 
     */

    for(Loop::block_iterator Biter = lp->block_begin(), Be = lp->block_end(); Biter != Be ; ++Biter)
    {
      BasicBlock *BB = *Biter; 
      for(BasicBlock::iterator iter = *BB->begin(), ie = *BB->end() ; iter != ie ; ++iter){
   
        // First instruction in first basic block of loop is always Load for iterator
        if(Biter == lp->block_begin() && iter == *BB->begin())
        {
          // TODO :  Need to use Exit block !! In case of while, do while, they don't have iterator
          // There could be multiple Exit blocks ... 
          // What about preheader??
        }
        else
        {
          inst = iter;

          // Does this expression access global variable?
          for(unsigned i=0;i < inst->getNumOperands();++i){
            child = inst->getOperand(i);
            if(isa<GlobalValue>(child) & !isa<Function>(child))
            {
              loop_useGlobalVar |= true;
              detect_report << "\t\tUse Global Variable : " << child->getName().str() << "\n";
            }
          }

          // Array, pointer
          if(isa<GetElementPtrInst>(iter))
            checkStateGatherScatter(iter, &loop_haveStateInput, &loop_gather_scatter);
          
          else if(isa<CallInst>(iter))
          {
            CallInst *call = dyn_cast<CallInst>(iter);
            Function *func = call->getCalledFunction();
            if(func){
              // Does it use I/O library function call?
              if(forbiddenFuncSet.count(func->getName()))
              {
                loop_haveSideEffect |= true;
                detect_report << "\t\tUse I/O library : " + func->getName().str() + "\n";
              }
              // Does it use unsafe function call?
              else
              {
                bool unsafeFunc = isUnsafeFunc(func);
                loop_callUnsafeFunc |= unsafeFunc;  // check if accessing user-defined function is safe or not

                //If it's safe call, check the dependency between iterations and gather/scatter pattern
                if(!unsafeFunc)
                {
                  for(unsigned i=0 ; i < iter->getNumOperands()-1 ; ++i) 
                  {
                    Instruction *opInst = dyn_cast <Instruction> (iter->getOperand(i));
                    if(opInst && isa<LoadInst>(opInst)){
                      Instruction *child = dyn_cast <Instruction> (opInst->getOperand(0));
                      if(child && isa<GetElementPtrInst>(child))
                        checkStateGatherScatter(child, &loop_haveStateInput, &loop_gather_scatter);
                    }
                    else //errs() << "Constant, Pointer(?)  Parameter!\n";
                      detect_report << "\t\tUse constant, pointer, paramter! -> Figure out what's going on : ";
                      detect_report << iter->getOperand(i)->getName().str() << "\n";
                  }
                }
              }
            }
          }
        }
        
        
      }
    }
    detect_report << "\t\tState / Global / Side Effect / Unsafe Func call : " << loop_haveStateInput << " " << loop_useGlobalVar << " " << loop_haveSideEffect << " " << loop_callUnsafeFunc <<"\n";
    //errs() << "State / Global / Side Effect / Unsafe Func call : " << loop_haveStateInput << " " << loop_useGlobalVar << " " << loop_haveSideEffect << " " << loop_callUnsafeFunc <<"\n";
    iterator_list.clear();
    loop_violate = loop_haveStateInput | loop_useGlobalVar | loop_haveSideEffect | loop_callUnsafeFunc;
    violate |= loop_violate;
    detectMapLoopSet.push_back(std::pair <Loop*, bool>(lp, loop_violate));
    detectGatherScatterLoopSet.push_back(std::pair <Loop*, bool>(lp, !loop_violate & loop_gather_scatter));

    if(loop_violate) 
      detect_loop << F.getName().str() << "," << loop_count << ",0\n";
    else if(!loop_gather_scatter)
      detect_loop << F.getName().str() << "," << loop_count << ",1\n";
    else
      detect_loop << F.getName().str() << "," << loop_count << ",2\n";

 }

  userDefinedFuncSet.push_back(std::pair<Function*, bool>(&F, violate));
  
  if(violate) 
    detect_func << F.getName().str() + ",0\n";
  else{
    if(!gather_scatter) 
      detect_func << F.getName().str() + ",1\n";
    else               
      detect_func << F.getName().str() + ",2\n";
  }

  detect_func.close();
  detect_loop.close();
  detect_report.close();
  return true;
}

