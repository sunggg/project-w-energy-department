#include "llvm/Pass.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Operator.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfo.h"


using namespace llvm;
namespace {

  struct reduction_detection: public FunctionPass{
    static char ID;

    
    reduction_detection() : FunctionPass(ID){}; //constructor
    virtual bool runOnFunction(Function &F);

    virtual void getAnalysisUsage(AnalysisUsage &AU) const{
      AU.addRequired<LoopInfo>();
    }
    using llvm::Pass::doFinalization;
    //virtual bool doFinalization();
    bool doFinalization(){
      return false;
    }
  }; 
  
}
char reduction_detection::ID = 0; // LLVM uses the address of ID. => value doesn't matter
static RegisterPass<reduction_detection> X("reduction_detection", "Detect Reduction type", false, false);
// command line arguement  /  name  /  walks CFG without modifying?   /  analysis pass(ex. dominator tree pass)?

/* NOTE : Choose conservative way to detect reduction type.
 * If there are more than one variables being generated inside a function, all of them should be reduction in order to be called as 'reduction type'.
 * This is because we will apply sort of approximation technique for guessing the value of reduction variables.
 * TODO : Expand it to detect reduction function
 * TODO : Fix errors on kmeans/segmentation.c
 */

/* NOTE : How to turn an iterator into a class pointer? => ex. BasicBlock::iterator it(inst) */

/* NOTE : OPCODE - Add : 8  sdiv : 15  sub : 10  mul : 12
 *
 */

int findOccurrence(Value* op, Value* target)
{
  int count = 0;
  if(isa<Instruction> (op))
  {
    Instruction* inst = dyn_cast<Instruction> (op);

    //errs() << "\nFind Occur op : " << *op;
    if(isa<LoadInst>(inst))
    {
      /* TODO  
       * Now it cannot detect the pattern like
       *    
       *   for
       *     a = c+4
       *     c += 1;
       *
       */
      int ret = (inst->getOperand(0) == target);

      if(ret)
        for(Value::use_iterator U = inst->use_begin() ; U != inst->use_end() ; U++)
        {
          Instruction* ui = dyn_cast <Instruction>(*U);
          //errs() << "\n  "<< ui->getOpcode() << " -> " << *ui << "\n";
          if(ui->getOpcode () == 12 || ui->getOpcode() == 15) ret += 2; // Any value which is larger than 1 - To make decision as 'not reduction type'
        }
      
      return ret;
    }
    return findOccurrence(inst->getOperand(0), target) + findOccurrence(inst->getOperand(1), target);
  }
  else
    return 0;
}

std::set <Instruction*> possibleRVset;
bool reduction_detection::runOnFunction(Function &F){
      LoopInfo &LI = getAnalysis<LoopInfo>();

      errs() << "\n\n" + F.getName() + "\n";
      errs() << "\n\nStart Loop investigation\n\n";
  
      for(LoopInfo::reverse_iterator l = LI.rbegin(), e = LI.rend() ; l != e ; ++l){
        Loop *lp = *l;
        for(Loop::block_iterator Biter = lp->block_begin(), Be = lp->block_end() ; Biter != Be ; ++Biter){
          BasicBlock *BB = *Biter;
          for(BasicBlock::iterator iter = *BB->begin(), ie = *BB->end(); iter != ie ; ++iter){
            //errs() << *iter << "\t[" << iter->getOpcode() <<"]\n";
            Instruction *inst = dyn_cast <Instruction> (iter);

            if(isa<StoreInst>(iter))
              possibleRVset.insert(inst);
            else if(isa<LoadInst>(iter))
              for(std::set<Instruction*>::iterator ii = possibleRVset.begin(), ie = possibleRVset.end() ; ii != ie ; ++ii)
                if( (*ii)->getOperand(1) == inst->getOperand(0)) possibleRVset.erase(ii);
          }
        }
        //errs() << "Possible RV set : ";
        for(std::set <Instruction*>::iterator it = possibleRVset.begin(), ie = possibleRVset.end(); it != ie ; ++it){
          if(findOccurrence((*it)->getOperand(0), (*it)->getOperand(1)) == 1)
            errs() << **it << " : reduction variable!\n";
          else
            errs() << **it << " : NOT reduction variable!\n";
          //errs() << **it << " ---> ";
          //errs() << findOccurrence((*it)->getOperand(0), (*it)->getOperand(1)) << "\n";
        }
        errs() << "\n";
        possibleRVset.clear();
      }
      
      errs() << "End of Function\n";

      return true;
}

