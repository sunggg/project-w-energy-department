from sklearn.metrics import mean_squared_error
from math import sqrt
from matplotlib import pyplot as plt
import numpy as np

feature_data = np.loadtxt("./raw_data/profile-data.1.av.in.csv",delimiter=",")
label_data = np.loadtxt("./raw_data/profile-data.1.av.out.csv",delimiter=",")


data_size = feature_data.shape[0]
#feature_data = feature_data.reshape([data_size,1])
#label_data = label_data.reshape([data_size,1])

feature_size = feature_data.shape[1]
label_size = label_data.shape[1]

train_size = data_size / 2
test_size = data_size - train_size

train_feature = feature_data[0:train_size, :]
train_label = label_data[0:train_size, :]

test_feature = feature_data[train_size:data_size, :]
test_label = label_data[train_size:data_size, :]

poly = 3;
lambda_value = 0.01;
 
for m in range(label_size):
  train_label_i = train_label[:,m:m+1]
  test_label_i = test_label[:,m:m+1]
  
  design_matrix = np.zeros((train_size,feature_size*poly+1))
  for n in range(poly):
    design_matrix[:,feature_size*n:feature_size*(n+1)] = np.power(train_feature,n+1)
  design_matrix[:,feature_size*poly:feature_size*poly+1] = np.ones((train_size,1))

  temp_matrix0 = np.dot(design_matrix.transpose(), design_matrix)
  temp_matrix1 = np.multiply(lambda_value, np.identity(design_matrix.shape[1]))
  temp_matrix2 = np.dot(design_matrix.transpose(), train_label_i)
  
  coeffs = np.linalg.solve(np.add(temp_matrix0, temp_matrix1), temp_matrix2)
  #coeffs = np.dot(np.linalg.pinv(design_matrix),train_label_i)
  predict_train_label = np.dot(design_matrix, coeffs)
  train_error = sqrt(mean_squared_error(train_label_i, predict_train_label))
  train_error = train_error / np.mean(np.absolute(train_label_i))
  print "ID: ", m, ", Train Error: ", train_error

  test_matrix = np.zeros((test_size,feature_size*poly+1))
  for n in range(poly):
    test_matrix[:,feature_size*n:feature_size*(n+1)] = np.power(test_feature,n+1)
  test_matrix[:,feature_size*poly:feature_size*poly+1] = np.ones((test_size,1))

  predict_test_label = np.dot(test_matrix, coeffs)
  test_error = sqrt(mean_squared_error(test_label_i, predict_test_label))
  test_error = test_error / np.mean(np.absolute(test_label_i))
  print "ID: ", m, ", Test Error: ", test_error
  
  fn = "1.coeffs_" + str(m) + ".txt"
  f = open(fn,'w')
  for i in range(coeffs.size):
    print >>f, coeffs[i]
  print >>f
