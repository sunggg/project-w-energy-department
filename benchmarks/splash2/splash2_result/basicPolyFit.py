from sklearn.linear_model import LinearRegression as LR
from sklearn.metrics import mean_squared_error
from math import sqrt
from matplotlib import pyplot as plt
import numpy as np

test_function_id = 6
method = "av"
infile_name = "./raw_data/profile-data."+str(test_function_id)+"."+method+".in.csv"
outfile_name = "./raw_data/profile-data."+str(test_function_id)+"."+method+".out.csv"

fn_error = "./coeffs/" + str(test_function_id) + ".error."+method+".txt"
fe = open(fn_error,'w')

feature_data = np.loadtxt(infile_name,delimiter=",")
label_data = np.loadtxt(outfile_name,delimiter=",")

data_size = feature_data.shape[0]
print "sample_size=",data_size
#feature_data = feature_data.reshape([data_size,1])
#label_data = label_data.reshape([data_size,1])

feature_size = feature_data.shape[1]
label_size = label_data.shape[1]

data_group = np.zeros([data_size, feature_size+label_size])
data_group[:,:feature_size] = feature_data
data_group[:,feature_size:] = label_data
np.random.shuffle(data_group)
feature_data = data_group[:,:feature_size]
label_data = data_group[:,feature_size:]

train_size = data_size / 2
test_size = data_size - train_size

train_feature = feature_data[0:train_size, :]
train_label = label_data[0:train_size, :]

test_feature = feature_data[train_size:data_size, :]
test_label = label_data[train_size:data_size, :]

'''
poly = 3;
lambda_value = 0.01;
'''
clf = LR()
clf.fit(train_feature,train_label)

predict_train_labels = clf.predict(train_feature)
predict_test_labels = clf.predict(test_feature)

for m in range(label_size):  
  '''
  train_label_i = train_label[:,m:m+1]
  test_label_i = test_label[:,m:m+1]
  
  design_matrix = np.zeros((train_size,feature_size*poly+1))
  for n in range(poly):
    design_matrix[:,feature_size*n:feature_size*(n+1)] = np.power(train_feature,n+1)
  design_matrix[:,feature_size*poly:feature_size*poly+1] = np.ones((train_size,1))

  temp_matrix0 = np.dot(design_matrix.transpose(), design_matrix)
  temp_matrix1 = np.multiply(lambda_value, np.identity(design_matrix.shape[1]))
  temp_matrix2 = np.dot(design_matrix.transpose(), train_label_i)
  
  coeffs = np.linalg.solve(np.add(temp_matrix0, temp_matrix1), temp_matrix2)
  #coeffs = np.dot(np.linalg.pinv(design_matrix),train_label_i)
  predict_train_label = np.dot(design_matrix, coeffs)
  '''
  predict_train_label = predict_train_labels[:,m]
  train_error = sqrt(mean_squared_error(train_label[:,m], predict_train_label))
  train_error = train_error / np.mean(np.absolute(train_label[:,m]))
  print "ID: ", m, ", Train Error: ", train_error
  print >>fe, "ID: ", m, ", Train Error: ", train_error

  '''
  test_matrix = np.zeros((test_size,feature_size*poly+1))
  for n in range(poly):
    test_matrix[:,feature_size*n:feature_size*(n+1)] = np.power(test_feature,n+1)
  test_matrix[:,feature_size*poly:feature_size*poly+1] = np.ones((test_size,1))
  
  predict_test_label = np.dot(test_matrix, coeffs)
  '''
  predict_test_label = predict_test_labels[:,m]
  test_error = sqrt(mean_squared_error(test_label[:,m], predict_test_label))
  test_error = test_error / np.mean(np.absolute(test_label[:,m]))
  
  print "ID: ", m, ", Test Error: ", test_error
  print >>fe, "ID: ", m, ", Test Error: ", test_error
  
  '''
  fn = "./coeffs/" + str(test_function_id) + ".coeffs_" + str(m) + "."+method+".txt"
  f = open(fn,'w')
  for i in range(coeffs.size):
    print >>f, coeffs[i]
  print >>f
  '''
