#!/bin/bash
CUDA_LIB=$(echo '/usr/local/cuda-7.0/samples/common/inc/')
fname=$(echo $2 | cut -d'.' -f1)

clang -emit-llvm $2 -c -o $fname.bc -I$CUDA_LIB

if [ "$1" == "-map" ]; then
  rm *.csv
  opt -load /home/sunggg/project/llvm-3.7.1/projects/pjt/Release+Asserts/lib/map_detection.so -map_detection <$fname.bc> /dev/null
elif [ "$1" == "-reduction" ]; then
  rm *.csv
  opt -load /home/sunggg/project/llvm-3.7.1/projects/pjt/Release+Asserts/lib/reduction_detection.so -reduction_detection <$fname.bc> /dev/null
elif [ "$1" == "-gen" ]; then
  opt -load /home/sunggg/project/llvm-3.7.1/projects/pjt/Release+Asserts/lib/genApprox.so -gen <$fname.bc> -o genApprox.bc
else 
  echo "Include option!"
fi
