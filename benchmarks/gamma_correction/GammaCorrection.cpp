/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */
// Includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
// includes, project
#include <helper_functions.h> // includes for SDK helper functions

typedef unsigned char Pixel;
void initializeData(char *file) ;

static int imWidth  = 0;   // Image width
static int imHeight = 0;   // Image height

unsigned char *pixels = NULL; 
#define GAMMA  5.0        // gamma value
#define COEFF_A 1.0

void runAutoTest(int argc, char **argv);

void initializeData(char *file)
{
    int bsize;
    unsigned int w, h;
    size_t file_length= strlen(file);

    if (!strcmp(&file[file_length-3], "pgm"))
    {
        if (sdkLoadPGM<unsigned char>(file, &pixels, &w, &h) != true)
        {
            printf("Failed to load PGM image file: %s\n", file);
            exit(EXIT_FAILURE);
        }
    }
    else if (!strcmp(&file[file_length-3], "ppm"))
    {
        if (sdkLoadPPM4(file, &pixels, &w, &h) != true)
        {
            printf("Failed to load PPM image file: %s\n", file);
            exit(EXIT_FAILURE);
        }

    }
    
    imWidth = (int)w;
    imHeight = (int)h;
}

void loadDefaultImage(char *loc_exec)
{

    printf("Reading image: lena.pgm\n");
    const char *image_filename = "lena.pgm";
    char *image_path = sdkFindFilePath(image_filename, loc_exec);

    if (image_path == NULL)
    {
        printf("Failed to read image file: <%s>\n", image_filename);
        exit(EXIT_FAILURE);
    }

    initializeData(image_path);
    free(image_path);
}

void gammaCorrection(Pixel *odata, int iw, int ih, unsigned char* orig_pixels)
{
  int size = iw;
  for(int i=0; i<iw; i++){
    for(int j=0; j<ih; j++){
      int idx = size*i+j;
      double dpx = (double)orig_pixels[idx];
      double px = COEFF_A*255.0*pow(dpx/255.0, GAMMA);
     
      odata[idx] = (unsigned char)(px);
   }
  } 
}


void runAutoTest(int argc, char *argv[])
{

    loadDefaultImage(argv[0]);

    Pixel *d_result = (unsigned char *)malloc(imWidth*imHeight*sizeof(Pixel));
    char  dump_file[256];

    sprintf(dump_file, "lena_gamma.pgm");
    gammaCorrection(d_result, imWidth, imHeight, pixels); //passing the global variable
    sdkSavePGM(dump_file, d_result, imWidth, imHeight);

    free(d_result);

    printf("Test passed!\n");
    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
    
  runAutoTest(argc, argv);
  return 0;
    
}
