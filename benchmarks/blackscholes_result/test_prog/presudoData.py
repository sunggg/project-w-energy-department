from sklearn.metrics import mean_squared_error
from math import sqrt
from math import pow
from matplotlib import pyplot as plt
import numpy as np

def poly1( x1, x2 ):
  y = 3.33 * pow(x1,3) - 5.21 * pow(x2, 2) + 1.72 * x1 - 0.12
  return y

def poly2( x1, x2 ):
  y = - 6.71 * pow(x2,3) + 8.91 * pow(x1, 2) + 5.12 * x2 + 0.03
  return y

fin = "presudoIn.csv"
fi = open(fin,'w')

fon = "presudoOut.csv"
fo = open(fon,'w')

rand1 = np.random.random(10000)
rand2 = np.random.random(10000)
rand1_noise = np.random.normal(0, 0.1, 10000)
rand2_noise = np.random.normal(0, 0.1, 10000)
rand1 = rand1 + rand1_noise
rand2 = rand2 + rand2_noise

for i in range(10000):
  si = str(rand1[i]) + "," + str(rand2[i])
  print >>fi, si
  so = str(poly1(rand1[i], rand2[i])) + "," + str(poly2(rand1[i], rand2[i]))
  print >>fo, so

